<?php
declare(strict_types=1);

use App\Application\Actions\Api\ViewTreeAction;
use Slim\App;

return function (App $app) {
    $app->get('/api/{user}/{repo}', ViewTreeAction::class);
};