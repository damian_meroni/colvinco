<?php
declare(strict_types=1);

namespace Tests\Application\Actions\Api;

use Tests\TestCase;

class ViewTreeActionTest extends TestCase
{
    public function testActionThrowsNotFound()
    {
        $app = $this->getAppInstance();

        $request = $this->createRequest('GET', 'api/symfony/symfony');
        $response = $app->handle($request);

        $payload = json_decode((string) $response->getBody(), true);

        $this->assertEquals(200, $payload["statusCode"]);
    }
}
