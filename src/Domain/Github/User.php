<?php
declare(strict_types=1);

namespace App\Domain\Github;

use JsonSerializable;

class User implements JsonSerializable
{
    private $username;
    private $pattern;

    /**
     * @param string    $username
     */
    public function __construct(string $username = "") {
        $this->username = $username;
        $this->pattern = "/[^a-zA-Z0-9-]/";
    }

    /**
     * @return string
     */
    public function getUsername(): string {
        return $this->username;
    }

    /**
     * @return array
     */
    public function jsonSerialize() {
        return [
            'username' => $this->username,
        ];
    }

     /**
     * @return array
     */
    public function serialize() {
        return [
            'username' => $this->username,
        ];
    }

    public function validate(): bool {
        return !preg_match($this->pattern, html_entity_decode($this->username, ENT_QUOTES));
    }
}