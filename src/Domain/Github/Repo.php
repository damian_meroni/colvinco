<?php
declare(strict_types=1);

namespace App\Domain\Github;

class Repo
{
    private $repository;
    private $pattern;

    /**
     * @param string    $repository
     */
    public function __construct(string $repository = "") {
        $this->repository = $repository;
        $this->pattern = "/[^a-zA-Z0-9-_]/";
    }

    /**
     * @return string
     */
    public function getRepository(): string {
        return $this->repository;
    }

    public function validate(): bool {
        return !preg_match($this->pattern, html_entity_decode($this->repository, ENT_QUOTES));
    }
}