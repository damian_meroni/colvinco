<?php
declare(strict_types=1);

namespace App\Application\Actions\Api;

use App\Domain\Github\User as User;
use App\Domain\Github\Repo as Repo;
use Slim\Exception\HttpBadRequestException;

use Psr\Http\Message\ResponseInterface as Response;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;

class ViewTreeAction extends ApiAction
{
    private $apiUrl;

    protected $user;
    protected $repo;

    public function __construct() {
        $this->apiUrl = "https://api.github.com/"; // Can be env var
    }

    /**
     * Get tree by User/repo
     * Some validations (like empty params) are taken by the fw 
     *
     * @return Response
     */
    protected function action(): Response {
        $newUser = new User($this->resolveArg('user'));

        if(!$newUser->validate()) {
            return $this->respondWithData("Invalid Github user name");
        }

        $newRepo = new Repo($this->resolveArg('repo'));

        if(!$newRepo->validate()) {
            return $this->respondWithData("Invalid Github repository name");
        }

        // cURL with Guzzler
        $client = new Client(['base_uri' => $this->apiUrl]);

        $userDataQuery = "repos/{$newUser->getUsername()}/{$newRepo->getRepository()}";

        // cURL to apiUrl with URI as second param
        try {
            $response = $client->request('GET', $userDataQuery);
        } catch (RequestException $e) {
            if ($e->getResponse()->getStatusCode() == '404') {
                throw new HttpBadRequestException($this->request, 'Repo not found.');
            }
        } catch (\Exception $e) {
            throw new HttpBadRequestException($this->request, 'Invalid response from API');
        }
        
        $repoInfo = $this->decodeResponse($response);

        if(!empty($repoInfo['default_branch'])) {
            $newQuery = $userDataQuery . "/git/trees/{$repoInfo['default_branch']}?recursive=1";

            try {
                $response = $client->request('GET', $newQuery);
                $responseDecoded = $this->decodeResponse($response);

                if(!empty($responseDecoded['tree'])) {
                    $getWordAppearance = $this->processList($responseDecoded['tree']);

                    return $this->respondWithData($getWordAppearance);
                }
            } catch (\Exception $e) {
                throw new HttpBadRequestException($this->request, 'Invalid response from API');
            }
        }

        return $this->respondWithData("Branch empty");
    }

    /**
     * Process list from json response
     *
     * @param array $fileTree
     * @return array
     */
    private function processList(array $fileTree): array {
        $wordListCount = [];

        foreach($fileTree as $filePath) {
            $fileName = basename($filePath['path']);

            // Is faster if we check ASCII against chars
            // We avoid use of preg_match and preg_match_all, instead we use char compare for optimization
            // And substr to get filetype for php
            if(substr($fileName, -4) === '.php' && ord($fileName{0}) > 64 && ord($fileName{0}) < 91 &&
            ord($fileName{1}) > 96 && ord($fileName{1}) < 123) {
                foreach(preg_split('/(?=[A-Z])/', substr($fileName, 0, -4), 0, PREG_SPLIT_NO_EMPTY) as $word) {
                    $wordListCount[$word] = isset($wordListCount[$word]) ? $wordListCount[$word] + 1 : 1;
                }
            }
        }

        return $wordListCount;
    }

    /**
     * Decode request
     *
     * @param Response $response
     * @return array|null
     */
    private function decodeResponse(Response $response): ?array {
        if($response->getStatusCode() !== 200)
            return null;
        
        $resDecode = json_decode((string) $response->getBody(), true);
        
        return (json_last_error() == JSON_ERROR_NONE) ? $resDecode : null;
    }
}