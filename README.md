ColvinCo PHP Test

Para correr la app en dev es necesario tener composer (https://getcomposer.org/download/)

ejecutar:

```JavaScript
composer install
```

Esperar que se instalen las dep. Seguido por:

```JavaScript
composer start
```

Para testear el endpoint una vez que este el servidor levantado:

http://localhost:8080/api/{user}/{repo}

Ej: http://localhost:8080/api/symfony/symfony

También se puede usar docker-compose:

```JavaScript
docker-compose up -d
```

Para correr los test:

```JavaScript
composer test
```

(No llegué a testear Docker)
